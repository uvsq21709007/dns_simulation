package fr.uvsq.dns.simul;

import fr.uvsq.dns.simul.UI.DnsTUI;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class TestDnsTUI
        extends TestCase {

    DnsTUI tui = new DnsTUI();
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public TestDnsTUI(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(TestDnsTUI.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testIP() {
        // test patern command IP adress
        assertTrue(tui.checkIP("192.68.36.255"));
    }

    public void testMachine() {
        // test patern command nom machine
        assertTrue(tui.checkNomMachine("nom.machine.dz"));
    }

    public void testListeDomaine() {
        // test patern command liste machine par domaine 
        assertTrue(tui.checkListeMachine("ls domaine.dz"));
    }
    public void testListeDomaineTriIP() {
        // test patern command liste machine par domaine 
        assertTrue(tui.checkListeMachineTriIP("ls[-a] domaine.dz"));
    }
}
