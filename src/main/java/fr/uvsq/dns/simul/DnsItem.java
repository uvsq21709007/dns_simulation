/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uvsq.dns.simul;

/**
 *
 * @author user
 */
public class DnsItem {

    private AdresseIP adresseIP = new AdresseIP();
    private NomMachine nomMachine = new NomMachine();

    public DnsItem() {
    }

    public DnsItem(String item) {
        String[] partsIpMachine = item.split("-", 2);
        adresseIP.setAdresseIP(partsIpMachine[0]);
        String[] partsMachine = partsIpMachine[1].split("\\.", 2);
        nomMachine.setNomQualif(partsMachine[0]);
        nomMachine.setDomaine(partsMachine[1]);
    }

    public AdresseIP getAdresseIP() {
        return adresseIP;
    }

    public void setAdresseIP(AdresseIP adresseIP) {
        this.adresseIP = adresseIP;
    }

    public NomMachine getNomMachine() {
        return nomMachine;
    }

    public void setNomMachine(NomMachine nomMachine) {
        this.nomMachine = nomMachine;
    }

    public String toStringIP() {
        return this.getAdresseIP().getAdresseIP() + " - " + this.getNomMachine().getNomQualif() + "." + this.getNomMachine().getDomaine();
    }

    public String toStringMachine() {
        return this.getNomMachine().getNomQualif() + "." + this.getNomMachine().getDomaine() + " - " + this.getAdresseIP().getAdresseIP();
    }
}
