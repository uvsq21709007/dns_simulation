/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uvsq.dns.simul;

/**
 *
 * @author user
 */
public class NomMachine {

    private String nomQualif;
    private String domaine;

    public NomMachine() {
    }

    public NomMachine(String commande) {
        String[] partsMachine = commande.split("\\.", 2);
        nomQualif = partsMachine[0];
        domaine = partsMachine[1];
    }

    public NomMachine(String nomQualif, String domaine) {
        this.nomQualif = nomQualif;
        this.domaine = domaine;
    }

    public String getNomQualif() {
        return nomQualif;
    }

    public void setNomQualif(String nomQualif) {
        this.nomQualif = nomQualif;
    }

    public String getDomaine() {
        return domaine;
    }

    public void setDomaine(String domaine) {
        this.domaine = domaine;
    }

}
