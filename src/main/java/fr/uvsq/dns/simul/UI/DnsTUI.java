/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uvsq.dns.simul.UI;

import fr.uvsq.dns.simul.AdresseIP;
import fr.uvsq.dns.simul.Dns;
import fr.uvsq.dns.simul.NomMachine;
import fr.uvsq.dns.simul.action.Commande;
import fr.uvsq.dns.simul.action.ErreurSyntaxe;
import fr.uvsq.dns.simul.action.Quitter;
import fr.uvsq.dns.simul.action.RechercherIP;
import fr.uvsq.dns.simul.action.RechercherMachineParDomaine;
import fr.uvsq.dns.simul.action.RechercherNom;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 *
 * @author user
 */
public class DnsTUI {

    private static final Pattern IP_PATTERN = Pattern.compile("^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");
    private static final Pattern Machine_PATTERN = Pattern.compile("[a-zA-z]+\\.[a-zA-z]+\\.[a-zA-z]+");
    private static final Pattern LISTE_PATTERN = Pattern.compile("ls [a-zA-z]+\\.[a-zA-z]+");
    private static final Pattern LISTE_TRI_IP_PATTERN = Pattern.compile("ls\\[\\-a\\] [a-zA-z]+\\.[a-zA-z]+");
    private final Dns dns = new Dns();

    private final Scanner scanner = new Scanner(System.in);
    private String s;

    public DnsTUI() {
    }

    public void interactionUser() {
        System.out.println(">");
        s = scanner.nextLine();
        while (1 == 1) {
            this.nextCommande(s).execute();
            System.out.println(">");
            s = scanner.nextLine();
        }
    }

    public boolean checkIP(String ip) {
        return IP_PATTERN.matcher(ip).matches();
    }

    public boolean checkNomMachine(String machine) {
        return Machine_PATTERN.matcher(machine).matches();
    }

    public boolean checkListeMachine(String liste) {
        return LISTE_PATTERN.matcher(liste).matches();
    }

    public boolean checkListeMachineTriIP(String liste) {
        return LISTE_TRI_IP_PATTERN.matcher(liste).matches();
    }

    private boolean checkQuitter(String quitter) {
        return quitter.toLowerCase().equals("exit");
    }

    public Commande nextCommande(String commande) {
        if (checkListeMachine(commande)) {
            String[] partsMachine = commande.split("\\s+", 2);
            return new RechercherMachineParDomaine(dns, partsMachine[1], false);
        }
        if (checkListeMachineTriIP(commande)) {
            String[] partsMachine = commande.split("\\s+", 2);
            return new RechercherMachineParDomaine(dns, partsMachine[1], true);
        }
        if (checkIP(commande)) {
            return new RechercherIP(dns, new AdresseIP(commande));
        }
        if (checkNomMachine(commande)) {
            return new RechercherNom(dns, new NomMachine(commande));
        }
        if (checkQuitter(commande)) {
            return new Quitter();
        }
        return new ErreurSyntaxe();
    }
}
