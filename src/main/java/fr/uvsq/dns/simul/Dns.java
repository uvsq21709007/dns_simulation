/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uvsq.dns.simul;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author user
 */
public class Dns {

    List<DnsItem> listeDnsItem = new ArrayList();

/**
 *
 * @author User
 */



    public Dns() {
        this.lireFichierDns();
    }

    private void lireFichierDns() {
        String fichier = "BD/BD.txt";
        try {
            InputStream ips = new FileInputStream(fichier);
            InputStreamReader ipsr = new InputStreamReader(ips);
            BufferedReader br = new BufferedReader(ipsr);
            String ligne;
            DnsItem item;
            while ((ligne = br.readLine()) != null) {
                item = new DnsItem(ligne);
                listeDnsItem.add(item);
            }
            br.close();
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }

   

    
     public DnsItem getItem(AdresseIP adrIp) {

        DnsItem item = new DnsItem();
        for (DnsItem i : listeDnsItem) {
            if (i.getAdresseIP().getAdresseIP().equals(adrIp.getAdresseIP())) {
                item = i;
                break;
            }
        }
        return item;
    }

    public DnsItem getItem(NomMachine machine) {
        DnsItem item = new DnsItem();
        for (DnsItem i : listeDnsItem) {
            if ((i.getNomMachine().getNomQualif() + "." + i.getNomMachine().getDomaine()).equals(machine.getNomQualif() + "." + machine.getDomaine())) {
                item = i;
                break;
            }
        }
        return item;
    }

    public List<String> getListeItemsByIP(String domaine) {
        List<String> ls = new ArrayList();
        for (DnsItem i : listeDnsItem) {
            if (i.getNomMachine().getDomaine().equals(domaine)) {
                ls.add(i.toStringIP());
            }
        }
        return ls;
    }


  

    
      public List<String> getListeItemsByMachine(String domaine) {

        List<String> ls = new ArrayList();
        for (DnsItem i : listeDnsItem) {
            if (i.getNomMachine().getDomaine().equals(domaine)) {
                ls.add(i.toStringMachine());
            }
        }
        return ls;
    }


}

