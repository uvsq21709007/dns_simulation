/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uvsq.dns.simul.action;

import fr.uvsq.dns.simul.AdresseIP;
import fr.uvsq.dns.simul.Dns;

/**
 *
 * @author user
 */
public class RechercherIP implements Commande {

    private final Dns dns;
    private final AdresseIP adresseIP;

    public RechercherIP(Dns dns, AdresseIP adresseIP) {
        this.dns = dns;
        this.adresseIP = adresseIP;
    }

    public void execute() {
        System.out.println("Machine -- > " + dns.getItem(adresseIP).getNomMachine().getNomQualif());
        System.out.print("." + dns.getItem(adresseIP).getNomMachine().getDomaine());
    }

}
