/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uvsq.dns.simul.action;

import fr.uvsq.dns.simul.Dns;
import fr.uvsq.dns.simul.NomMachine;

/**
 *
 * @author user
 */
public class RechercherNom implements Commande {

    private final Dns dns;
    private final NomMachine nomMachine;

    public RechercherNom(Dns dns, NomMachine nomMachine1) {
        this.dns = dns;
        this.nomMachine = nomMachine1;
    }

    public void execute() {
        System.out.println("IP -- > " + dns.getItem(nomMachine).getAdresseIP().getAdresseIP());
    }

}
