/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uvsq.dns.simul.action;

import fr.uvsq.dns.simul.Dns;
import fr.uvsq.dns.simul.DnsItem;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author user
 */
public class RechercherMachineParDomaine implements Commande {

    private final Dns dns;
    private final String domaine;
    private final boolean triParIP;

    public RechercherMachineParDomaine(Dns dns, String domaine, boolean triParIP) {
        this.dns = dns;
        this.domaine = domaine;
        this.triParIP = triParIP;
    }

    public void execute() {
        System.out.println(" machines : ");
        if (triParIP) { //trier par IP
            List<String> listeItemIP = dns.getListeItemsByIP(domaine);
            Collections.sort(listeItemIP);
            for (String i : listeItemIP) {
                System.out.println(i);
            }
        } else { //trier par machine
            List<String> listeItemMachine = dns.getListeItemsByMachine(domaine);
            Collections.sort(listeItemMachine);
            for (String i : listeItemMachine) {
                System.out.println(i);
            }
        }
    }

}
