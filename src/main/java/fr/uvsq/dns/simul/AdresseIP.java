/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uvsq.dns.simul;

/**
 *
 * @author user
 */
public class AdresseIP {

    private String adresseIP;

    public AdresseIP() {
    }

    public AdresseIP(String adresseIP) {
        this.adresseIP = adresseIP;
    }

    public String getAdresseIP() {
        return adresseIP;
    }

    public void setAdresseIP(String adresseIP) {
        this.adresseIP = adresseIP;
    }

}
